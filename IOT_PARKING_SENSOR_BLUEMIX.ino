#include <Bridge.h>
#include <YunClient.h>
#include <PubSubClient.h>

#define CAR_PARK_SENSOR 1
#define CAR_COUNT_SENSOR 2

//Organization related Server Details
String org = "e4eeeh";
char IOTMQServer[] = "e4eeeh.messaging.internetofthings.ibmcloud.com";
int IOTMQPort = 1883;
char IOTCmdQueue[] = "iot-2/cmd/devcmd/fmt/String";
char IOTEvtStatusQueue[] = "iot-2/evt/status/fmt/json";

//Device Credentials and Config
char clientId[] = "d:e4eeeh:CAR_PARK_SENSOR:b4218af00a93";
String device_id = "b4218af00a93";//"b4218af00a93";//b4218af00a102;
String id_no = "San Fernando Street Parking 1";
char apiKey[] = "use-token-auth";
char apiToken[] = "b4218af00a93";
double latitude = 37.33744;
double longitude = -121.8845287;
int polling_interval = 2000;
int rate = 10;
String device_type = "CAR_PARK_SENSOR";
int current_device_type = CAR_PARK_SENSOR;
int capacity = 1;
int empty_spots = 1;

//Device Initial BroadCast State
boolean broadcast = false;

//Sensor Configurations - 1
const int trigPin1 = 2;
const int echoPin1 = 3;

//Sensor Configurations - 2
const int trigPin2 = 6;
const int echoPin2 = 7;

//Server Connections;
YunClient yunClient;
PubSubClient mqtt(IOTMQServer, IOTMQPort, callback, yunClient);

//Sensor Settings
unsigned long lastPublishedTime;
unsigned long lastSensor1PingTime;
unsigned int pingInterval = 2000;
String payload;

//Distance data variables
int inParkSensor = 0;
int outParkSensor = 0;
int thresholdPC = 10;
int thresholdParkingSpots = 10;

//parking Sensor State
int SYSTEM_STATE = 0; // 0- Default || 1-inParkSensor "Blocked" || 2-outParkSensor "Blocked"

void setup()
{
  pinMode(13, OUTPUT);
  digitalWrite(13, HIGH);
  Serial.begin(9600);

  //Setup the First Sensor
  pinMode(trigPin1, OUTPUT);
  pinMode(echoPin1, INPUT);

  //Setup the Second Sensor
  pinMode(trigPin2, OUTPUT);
  pinMode(echoPin2, INPUT);
  
  Bridge.begin();
  mqtt.connect(clientId, apiKey, apiToken);
  mqtt.subscribe(IOTCmdQueue);
  broadcastMessage();
  digitalWrite(13, LOW);
}

void loop()
{
  switch (current_device_type) {
    case CAR_PARK_SENSOR :
      carParkSensor();
      break;
    case CAR_COUNT_SENSOR :
      carCountSensor();
      break;
    default: 
    break;
  }
  if (millis() > (lastPublishedTime + polling_interval) && broadcast)
  {
    lastPublishedTime = millis();
    broadcastMessage();
  }
  mqtt.loop();
}

void carParkSensor() {
  if (millis() >  lastSensor1PingTime + pingInterval && broadcast) {
    lastSensor1PingTime = millis();
    pingSensor(1);
    pingSensor(2);

    Serial.print("\n==> Street Parking Status:");
    Serial.println(empty_spots);

    //If Object is a CAR  i.e. it blocks the sensor
    if (inParkSensor <= thresholdPC && outParkSensor <= thresholdPC) {
      if (empty_spots == capacity) {
        empty_spots--;
      }
    }

    //If an Object is Leaving the parking location
    if (inParkSensor > thresholdPC && outParkSensor > thresholdPC) {
      if (empty_spots < capacity) {
        empty_spots++;
      }
    }
  }
}

void carCountSensor() {
  if ( millis() >  lastSensor1PingTime + pingInterval && broadcast) {
    lastSensor1PingTime = millis();
    pingSensor(1);
    delayMicroseconds(5);
    pingSensor(2);

    //Default State if Both the Sensors are no blocked!!
    if (inParkSensor > thresholdPC && outParkSensor > thresholdPC) {
      SYSTEM_STATE = 0;
    }

    Serial.print("\n==> CAR Entered.. Empty Spots:: ");
    Serial.println(empty_spots);
    Serial.print("Current State is ==>");
    Serial.println(SYSTEM_STATE);

    //If Object Entering into the Garaage and blocks outParkSensor
    if (SYSTEM_STATE == 0 && outParkSensor <= thresholdPC && inParkSensor > thresholdPC) {
      SYSTEM_STATE = 2;
    }

    //If Object is a CAR  i.e. it block boths the sensor at the same time.
    if (SYSTEM_STATE == 2 && outParkSensor <= thresholdPC && inParkSensor <= thresholdPC) {
      if (empty_spots > 0) {
        empty_spots--;
      }
      SYSTEM_STATE = 0;
    }

    //if an Object is Leaving the garage and blocks inParkSensor
    if (SYSTEM_STATE == 0 &&  inParkSensor <= thresholdPC && outParkSensor > thresholdPC) {
      SYSTEM_STATE = 1;
    }

    //If Object is a CAR  i.e. it block boths the sensor at the same time.
    if (SYSTEM_STATE == 1 &&  inParkSensor <= thresholdPC && outParkSensor <= thresholdPC) {
      if (empty_spots < capacity) {
        empty_spots++;
      }
      SYSTEM_STATE = 0;
    }
  }
}

void broadcastMessage() {
  //payload = "{\"d\":";
  payload = "{\"_id\":\"" + device_id + "\"";
  payload += ",\"device_id\":\"" + device_id + "\"";
  payload += ",\"device_type\":\"" + String(device_type) + "\"";
  payload += ",\"apiToken\":\"" + String(apiToken) + "\"";
  payload += ",\"capacity\":" + String(capacity);
  payload += ",\"empty_spots\":" + String(empty_spots);
  payload += ",\"latitude\":" + String(latitude, 8);
  payload += ",\"longitude\":" + String(longitude, 8);
  payload += ",\"org\":\"" + String(org) + "\"";
  payload += ",\"rate\":" + String(rate);
  //payload += ",\"virtual_sensor\": \"false\"";
  payload += ",\"id_no\":\"" + String(id_no) +  "\"}";
  //payload += "}";

  Serial.println(payload.length() + 1);
  char payloadChars[payload.length() + 1];
  payload.toCharArray(payloadChars, payload.length() + 1);
  mqtt.publish(IOTEvtStatusQueue, payloadChars);
  Serial.println("Published to the Queue");
  Serial.println(payloadChars);
}

long getDistance(unsigned int trigPin, unsigned int ecPin) {
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(5);
  digitalWrite(trigPin, LOW);
  long duration = pulseIn(ecPin, HIGH);
  return (duration / 29 / 2);
}

int pingSensor(unsigned int sensor) {
  switch (sensor) {
    case 1:
      inParkSensor = getDistance(trigPin1, echoPin1);
      Serial.print("Current inParkSensor ==> ");
      Serial.println(outParkSensor);
      break;

    case 2:
      outParkSensor = getDistance(trigPin2, echoPin2);
      Serial.print("Current outParkSensor ==> ");
      Serial.println(inParkSensor);
      break;

    default:
      break;
  }
}

void callback(char* topic, byte* payload, unsigned int length) {
  String commandType = "";
  String command = "";
  boolean cmdFound = false;
  char curr;
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  Serial.println();

  for (int i = 0; i < length; i++) {
    command += (char)payload[i];
  }
  command.trim();

  if (command == "stopBroadcast") {
    empty_spots = 0;
    broadcastMessage();
    Serial.println("Stop Broadcast Command Recieved!!");
    broadcast = false;
  }
  if (command == "startBroadcast") {
    empty_spots = capacity;
    broadcastMessage();
    Serial.println("Start Broadcast Command Recieved!!");
    broadcast = true;
  }
  if (command.startsWith("setPollingIntr")) {
    Serial.println("setPollingIntr Command Recieved!!");
    command = command.substring(command.indexOf('/') + 1, command.length());
    polling_interval = command.toInt();
    broadcastMessage();
  }
  if (command.startsWith("setRate")) {
    Serial.println("Setting Rate");
    command = command.substring(command.indexOf('/') + 1, command.length());
    rate = command.toInt();
    broadcastMessage();
  }
  if (command.startsWith("setCapacity")) {
    Serial.println("Setting Capacity");
    command = command.substring(command.indexOf('/') + 1, command.length());
    capacity = command.toInt();
    broadcastMessage();
  }
}

